<?php

/**
 * @file
 * Veritrans admin page configuration
 */
function uc_veritrans_admin_settings_form($form, &$form_state) {

  $form['veritrans'] = array(
    '#type' => 'fieldset',
    '#title' => t('Veritrans Payment Configuration Page'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['veritrans']['uc_veritrans_bni'] = array(
    '#type' => 'fieldset',
    '#title' => t('BNI Veritrans'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['veritrans']['uc_veritrans_bni']['uc_veritrans_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('BNI Veritrans Merchant ID'),
    '#default_value' => variable_get('uc_veritrans_merchant_id', ''),
    '#description' => t('BNI Veritrans merchant id used to access API'),
    '#size' => 75,
  );

  $form['veritrans']['uc_veritrans_bni']['uc_veritrans_merchant_hash_key'] = array(
    '#type' => 'textfield',
    '#title' => t('BNI Veritrans Merchant Hash Key'),
    '#default_value' => variable_get('uc_veritrans_merchant_hash_key', ''),
    '#description' => t('BNI Veritrans merchant hash key used to access API'),
    '#size' => 75,
  );

  $form['veritrans']['uc_veritrans_bni']['uc_veritrans_payment_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('BNI Veritrans Payment API URL'),
    '#description' => t('BNI Veritrans payment API URL'),
    '#default_value' => variable_get('uc_veritrans_payment_server_url', ''),
    '#size' => 75,
  );

  /*
    $form['veritrans']['uc_veritrans_bni']['uc_veritrans_payment_return_url'] = array(
    '#type' => 'textfield',
    '#title' => t('BNI Veritrans Payment Return URL'),
    '#description' => t('BNI Veritrans payment return URL'),
    '#default_value' => variable_get('uc_veritrans_payment_return_url', ''),
    '#size' => 75,
    );
   */

  $form['veritrans']['uc_veritrans_mandiri'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mandiri Veritrans'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['veritrans']['uc_veritrans_mandiri']['uc_veritrans_mandiri_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandiri Veritrans Merchant ID'),
    '#default_value' => variable_get('uc_veritrans_mandiri_merchant_id', ''),
    '#description' => t('Mandiri Veritrans merchant id used to access API'),
    '#size' => 75,
  );

  $form['veritrans']['uc_veritrans_mandiri']['uc_veritrans_mandiri_merchant_hash_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandiri Veritrans Merchant Hash Key'),
    '#default_value' => variable_get('uc_veritrans_mandiri_merchant_hash_key', ''),
    '#description' => t('Mandiri Veritrans merchant hash key used to access API'),
    '#size' => 75,
  );

  $form['veritrans']['uc_veritrans_mandiri']['uc_veritrans_mandiri_payment_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandiri Veritrans Payment API URL'),
    '#description' => t('Mandiri Veritrans payment API URL'),
    '#default_value' => variable_get('uc_veritrans_mandiri_payment_server_url', ''),
    '#size' => 75,
  );

  /*
    $form['veritrans']['uc_veritrans_mandiri']['uc_veritrans_mandiri_payment_return_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Mandiri Veritrans Payment Return URL'),
    '#description' => t('Mandiri Veritrans payment return URL'),
    '#default_value' => variable_get('uc_veritrans_mandiri_payment_return_url', ''),
    '#size' => 75,
    );
   */

  return system_settings_form($form);
}
